package HW1;

/**
 * Created by TROUBLE on 09.05.16.
 */
public class Car {
    String company;
    String model;
    int speed;

    Car(String company, String model, int speed) {
        this.company = company;
        this.model = model;
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Класс Car {" +
                "компания-производитель='" + company + '\'' +
                ", модель='" + model + '\'' +
                ", скорость=" + speed +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (speed != car.speed) return false;
        if (!company.equals(car.company)) return false;
        return model.equals(car.model);
    }

    @Override
    public int hashCode() {
        int result = company.hashCode();
        result = 31 * result + model.hashCode();
        result = 31 * result + speed;
        return result;
    }
}