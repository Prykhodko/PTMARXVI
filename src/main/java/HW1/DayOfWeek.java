package HW1;

/**
 * Created by TROUBLE on 08.05.16.
 */
public enum DayOfWeek {
    MONDAY(0) {
        @Override
        public DayOfWeek forward1() {
            return TUESDAY;
        }
        @Override
        public DayOfWeek forward2() {
            return WEDNESDAY;
        };
        @Override
        public DayOfWeek forward3() {
            return THURSDAY;
        };
        @Override
        public DayOfWeek forward4() {
            return FRIDAY;
        };
        @Override
        public DayOfWeek forward5() {
            return SATURDAY;
        };
        @Override
        public DayOfWeek forward6() {
            return SUNDAY;
        };
    }, TUESDAY(1) {
        @Override
        public DayOfWeek forward1() {
            return WEDNESDAY;
        }
        @Override
        public DayOfWeek forward2() {
            return THURSDAY;
        };
        @Override
        public DayOfWeek forward3() {
            return FRIDAY;
        };
        @Override
        public DayOfWeek forward4() {
            return SATURDAY;
        };
        @Override
        public DayOfWeek forward5() {
            return SUNDAY;
        };
        @Override
        public DayOfWeek forward6() {
            return MONDAY;
        };

    }, WEDNESDAY(2) {
        @Override
        public DayOfWeek forward1() {
            return THURSDAY;
        }
        @Override
        public DayOfWeek forward2() {
            return FRIDAY;
        };
        @Override
        public DayOfWeek forward3() {
            return SATURDAY;
        };
        @Override
        public DayOfWeek forward4() {
            return SUNDAY;
        };
        @Override
        public DayOfWeek forward5() {
            return MONDAY;
        };
        @Override
        public DayOfWeek forward6() {
            return TUESDAY;
        };

    }, THURSDAY(3) {
        @Override
        public DayOfWeek forward1() {
            return FRIDAY;
        }
        @Override
        public DayOfWeek forward2() {
            return SATURDAY;
        };
        @Override
        public DayOfWeek forward3() {
            return SUNDAY;
        };
        @Override
        public DayOfWeek forward4() {
            return MONDAY;
        };
        @Override
        public DayOfWeek forward5() {
            return TUESDAY;
        };
        @Override
        public DayOfWeek forward6() {
            return WEDNESDAY;
        };

    }, FRIDAY(4) {
        @Override
        public DayOfWeek forward1() {
            return SATURDAY;
        }
        @Override
        public DayOfWeek forward2() {
            return SUNDAY;
        };
        @Override
        public DayOfWeek forward3() {
            return MONDAY;
        };
        @Override
        public DayOfWeek forward4() {
            return TUESDAY;
        };
        @Override
        public DayOfWeek forward5() {
            return WEDNESDAY;
        };
        @Override
        public DayOfWeek forward6() {
            return THURSDAY;
        };

    }, SATURDAY(5) {
        @Override
        public DayOfWeek forward1() {
            return SUNDAY;
        }
        @Override
        public DayOfWeek forward2() {
            return MONDAY;
        };
        @Override
        public DayOfWeek forward3() {
            return TUESDAY;
        };
        @Override
        public DayOfWeek forward4() {
            return WEDNESDAY;
        };
        @Override
        public DayOfWeek forward5() {
            return THURSDAY;
        };
        @Override
        public DayOfWeek forward6() {
            return FRIDAY;
        };
    }, SUNDAY(6) {
        @Override
        public DayOfWeek forward1() {
            return MONDAY;
        }
        @Override
        public DayOfWeek forward2() {
            return TUESDAY;
        };
        @Override
        public DayOfWeek forward3() {
            return WEDNESDAY;
        };
        @Override
        public DayOfWeek forward4() {
            return THURSDAY;
        };
        @Override
        public DayOfWeek forward5() {
            return FRIDAY;
        };
        @Override
        public DayOfWeek forward6() {
            return SATURDAY;
        };
    };

    private int index;

    DayOfWeek(int index) {
        this.index = index;
    }

    //абстрактные методы для изменения дня недели
    public abstract DayOfWeek forward1();
    public abstract DayOfWeek forward2();
    public abstract DayOfWeek forward3();
    public abstract DayOfWeek forward4();
    public abstract DayOfWeek forward5();
    public abstract DayOfWeek forward6();
}
