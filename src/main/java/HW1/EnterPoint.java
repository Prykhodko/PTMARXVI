package HW1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by TROUBLE on 09.05.16.
 */
public class EnterPoint {
    public static void main(String[] args) {
        List<HW1.Car> carList = new ArrayList<>();

        HW1.Car car1 = new HW1.Car("Mersedez", "S150", 280);
        HW1.Car car2 = new HW1.Car("Wolksvagen", "B14", 230);
        HW1.Car car3 = new HW1.Car("Opel", "Star15", 250);
        HW1.Car car4 = new HW1.Car("ZAZ", "9", 180);
        HW1.Car car5 = new HW1.Car("Bentley", "S150", 320);

        carList.add(car1);
        carList.add(car2);
        carList.add(car3);
        carList.add(car4);
        carList.add(car5);

        System.out.println("Печать коллекции Car до преобразований");
        for (HW1.Car car: carList) System.out.println(car);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`");

        //Сортировка по возрастанию скорости
        Collections.sort(carList, new Comparator<HW1.Car>() {
            @Override
            public int compare(HW1.Car car1, HW1.Car car2) {
                return car1.speed - car2.speed;
            }
        });
        System.out.println("Сортировка по возрастанию");
        for (HW1.Car car: carList) System.out.println(car);

        Collections.sort(carList, new Comparator<HW1.Car>() {
            @Override
            public int compare(HW1.Car car1, HW1.Car car2) {
                return car2.speed - car1.speed;
            }
        });

        System.out.println("Сортировка по убыванию");
        for (HW1.Car car: carList) System.out.println(car);



        HW1.Car car6 = new HW1.Car("Shkoda", "MP17", 270) {
            @Override
            public String toString() {
                return "Мне больше всего нравится " + company + ", модель - " + model + ", которая развивает скорость - " + speed + "км\\ч";
            }

            public boolean equals(HW1.Car car) {
                if (this == car) return true;
                if (car == null || this.getClass() != car.getClass()) return false;

                if (this.company == car.company) {
                    return true;
                }
                return false;
            }
        };

        HW1.Car car7 = new HW1.Car("Aston Martin", "007", 420) {
            @Override
            public String toString() {
                return "Джеймс Бонд ездил на " + company + "модели " + model + "с умопомрачительной скоростью - " + speed + "км\\ч";
            }

            public boolean equals(HW1.Car car) {
                if (this == car) return true;
                if (car == null || this.getClass() != car.getClass()) return false;

                if (this.company == car.company && this.model == car.model) {
                    return true;
                }
                return false;
            }
        };

        carList.add(car6);
        carList.add(car7);

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`");

        for (HW1.Car car: carList) System.out.println(car);
    }

}
