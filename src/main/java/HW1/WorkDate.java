package HW1;

/**
 * Created by TROUBLE on 01.05.16.
 */
public class WorkDate {

    private Year year;
    private Month month;
    private Day day;
    private DayOfWeek dow = DayOfWeek.MONDAY;

    WorkDate (int year, int month, int day) {
        this.year = new Year(year);
        this.month = new Month(month, this.year);
        this.day = new Day(day, this.month);
        dow = setDayOfWeek(year, month, day);
    }

    public DayOfWeek getDayOfWeek() {
        return dow;
    }

    public String getStringDayOfWeek() {
        String stringDayOfWeek = null;
        switch (dow) {
            case MONDAY: stringDayOfWeek = "Monday"; break;
            case TUESDAY: stringDayOfWeek = "Tuesday"; break;
            case WEDNESDAY: stringDayOfWeek = "Wednesday"; break;
            case THURSDAY: stringDayOfWeek = "Thursday"; break;
            case FRIDAY: stringDayOfWeek = "Friday"; break;
            case SATURDAY: stringDayOfWeek = "Saturday"; break;
            case SUNDAY: stringDayOfWeek = "Sunday"; break;
        }
        return stringDayOfWeek;
    }

    private DayOfWeek setDayOfWeek(int year, int month, int day) {
        //1 января 1900 года - воскресенье
        DayOfWeek dayOfWeek = DayOfWeek.MONDAY;
        //рассчитаем какой день недели 1 января year года.
        // Если год высокосный, то день недели смещается на 2 позиции вперед (т.к. 366%7 = 2),
        // если не высокосный, то на 1 позици вперед (т.к. 365%7 = 1)
        for (int i = 0; i < (year - 1900); i++) {
            if (isLeap(1900+i)) {
                dayOfWeek = dayOfWeek.forward2();
            } else {
                dayOfWeek = dayOfWeek.forward1();
            }
        }
        //определим сколько дней от 1 января year года до выбранной даты
        int shift = (getDayOfYear(year, month, day)-1)%7;
        switch (shift) {
            case 1: dayOfWeek = dayOfWeek.forward1(); break;
            case 2: dayOfWeek = dayOfWeek.forward2(); break;
            case 3: dayOfWeek = dayOfWeek.forward3(); break;
            case 4: dayOfWeek = dayOfWeek.forward4(); break;
            case 5: dayOfWeek = dayOfWeek.forward5(); break;
            case 6: dayOfWeek = dayOfWeek.forward6(); break;
        }
        return dayOfWeek;
    }

    //Сначала находим кол-во дней в зависимости от того, какой по счету месяц,
    //затем добавляем высокосный день, если год высокосный и месяц позже февраля, потом добавляем кол-во дней.
    public static int getDayOfYear(WorkDate wd) {
        //переменная, которая хранит кол-во дней.
        int dayOfYear = 0;
        switch (wd.month.month) { //31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};
            case 1: dayOfYear = 0; break;
            case 2: dayOfYear = 31; break;
            case 3: dayOfYear = 59; break;
            case 4: dayOfYear = 90; break;
            case 5: dayOfYear = 120; break;
            case 6: dayOfYear = 151; break;
            case 7: dayOfYear = 181; break;
            case 8: dayOfYear = 212; break;
            case 9: dayOfYear = 243; break;
            case 10: dayOfYear = 273; break;
            case 11: dayOfYear = 304; break;
            case 12: dayOfYear = 334; break;
        }
        if (wd.year.leap && wd.month.month > 2) {
            dayOfYear = dayOfYear+1;
        }
        dayOfYear = dayOfYear + wd.day.day;
        return dayOfYear;
    }

    //Перегруженный метод
    public int getDayOfYear() {
        return getDayOfYear(WorkDate.this);
    }

    //Второй перегруженный метод для метода определения дня недели
    public static int getDayOfYear(int year, int month, int day) {
        //переменная, которая хранит кол-во дней.
        int dayOfYear = 0;
        switch (month) { //31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};
            case 1: dayOfYear = 0; break;
            case 2: dayOfYear = 31; break;
            case 3: dayOfYear = 59; break;
            case 4: dayOfYear = 90; break;
            case 5: dayOfYear = 120; break;
            case 6: dayOfYear = 151; break;
            case 7: dayOfYear = 181; break;
            case 8: dayOfYear = 212; break;
            case 9: dayOfYear = 243; break;
            case 10: dayOfYear = 273; break;
            case 11: dayOfYear = 304; break;
            case 12: dayOfYear = 334; break;
        }
        if (isLeap(year) && month > 2) {
            dayOfYear = dayOfYear+1;
        }
        dayOfYear = dayOfYear + day;
        return dayOfYear;
    }

    //возвращает кол-во дней с конца года до определенной даты
    public static int oppositeGetDayOfYear (WorkDate wd) {
        int dayOfYear = 0;
        //switch для быстрого определения кол-ва дней по месяцу без учета высокосносности
        switch (wd.month.month) { //[31, 61, 92, 122, 153, 184, 214, 245, 275, 306, 334, 365]
            case 12: dayOfYear = 0; break;
            case 11: dayOfYear = 31; break;
            case 10: dayOfYear = 61; break;
            case 9: dayOfYear = 92; break;
            case 8: dayOfYear = 122; break;
            case 7: dayOfYear = 153; break;
            case 6: dayOfYear = 184; break;
            case 5: dayOfYear = 214; break;
            case 4: dayOfYear = 245; break;
            case 3: dayOfYear = 275; break;
            case 2: dayOfYear = 306; break;
            case 1: dayOfYear = 334; break;
        }
        //если год высокосный прибавляем 1
        if (wd.year.leap && wd.month.month < 2) {
            dayOfYear = dayOfYear+1;
        }
        // прибавляем кол-во дней
        dayOfYear = dayOfYear + getDays(wd.month.month, wd.year.leap) - wd.day.day + 1;

        return dayOfYear;
    }

    //Перегруженный метод
    public int oppositeGetDayOfYear() {
        return oppositeGetDayOfYear(WorkDate.this);
    }

    //Метод возвращает кол-во дней между 2 датами. Если дата 2 больше 1 число положительное,
    //если наоборот, отрицательное, исли даты равны, возвращает 0.
    public static int daysBetween(WorkDate wd1, WorkDate wd2) {
        int qDays = 0;

        //Сначала считаем если года не совпадают
        if (wd1.year.year != wd2.year.year) {
            //quantity days in years between two dates
            for (int i = 1; i < Math.abs(wd1.year.year - wd2.year.year); i++) {
                if (WorkDate.isLeap(Math.min(wd1.year.year, wd2.year.year)+i)) {
                    qDays = qDays + 366;
                } else {
                    qDays = qDays + 365;
                }
            }
            //quantity days from 01.01 to MaxDate
            qDays = qDays + getDayOfYear(maxDate(wd1, wd2));
            //quantity days from 31.12 to MinDate
            qDays = qDays + oppositeGetDayOfYear(minDate(wd1, wd2)) - 1;

        //Считаем разницу если не совпадают месяца
        } else if (wd1.month.month != wd2.month.month) {
            //прибавляем в цикле все дни месяцов между этими 2 датами
            for (int i = 1; i < Math.abs(wd1.month.month - wd2.month.month); i++) {
                qDays = qDays + getDays(minDate(wd1, wd2).month.month + i, minDate(wd1, wd2).year.leap);
            }
            //прибавляем кол-во дней в большей дате
            qDays = qDays + maxDate(wd1, wd2).day.day;
            //прибавляем кол-во дней от конца месяца меньшей даты до меньшей даты
            qDays = qDays + getDays(minDate(wd1, wd2).month.month, minDate(wd1, wd2).year.leap) - minDate(wd1, wd2).day.day;

        //Считаем разницу если месяца совпадают
        } else if (wd1.day.day != wd2.day.day) {
            qDays = qDays + maxDate(wd1, wd2).day.day - minDate(wd1, wd2).day.day;
        }

        //определяем знак выражения с учетом следующего:
        //wd1 - стартовая дата, wd2 - конечная дата.
        if (largerDate(wd1, wd2) < 0) {
            qDays = qDays * -1;
        }

        return qDays;
    }

    //Перегруженый метод
    public int daysBetween(WorkDate workDate) {
        return daysBetween(WorkDate.this, workDate);
    }

    //метод, который возвращает большую из 2 дат. Если даты равны возвращает первую.
    private static WorkDate maxDate(WorkDate wd1, WorkDate wd2) {
        if (wd1.year.year != wd2.year.year) {
            if (wd1.year.year > wd2.year.year) {
                return wd1;
            } else {
                return wd2;
            }
        } else if (wd1.month.month != wd2.month.month) {
            if (wd1.month.month > wd2.month.month) {
                return wd1;
            } else {
                return wd2;
            }
        } else if (wd1.day.day != wd2.day.day) {
            if (wd1.day.day > wd2.day.day) {
                return wd1;
            } else {
                return wd2;
            }
        } else {
            return wd1;
        }
    }

    //метод, который возвращает меньшую из 2 дат. Если даты равны возвращает первую.
    private static WorkDate minDate (WorkDate wd1, WorkDate wd2) {
        if (wd1.year.year != wd2.year.year) {
            if (wd1.year.year > wd2.year.year) {
                return wd2;
            } else {
                return wd1;
            }
        } else if (wd1.month.month != wd2.month.month) {
            if (wd1.month.month > wd2.month.month) {
                return wd2;
            } else {
                return wd1;
            }
        } else if (wd1.day.day != wd2.day.day) {
            if (wd1.day.day > wd2.day.day) {
                return wd2;
            } else {
                return wd1;
            }
        } else {
            return wd1;
        }
    }

    //метод для определения большей даты. Если вторая дата больше - возвращает 1,
    //если меньше возвращает -1, если даты равны, возвращает 0.
    public static int largerDate (WorkDate wd1, WorkDate wd2) {
        if (wd1.year.year != wd2.year.year) {
            if (wd1.year.year < wd2.year.year) {
                return 1;
            } else {
                return -1;
            }
        } else if (wd1.month.month != wd2.month.month) {
            if (wd1.month.month < wd2.month.month) {
                return 1;
            } else {
                return -1;
            }
        } else if (wd1.day.day != wd2.day.day) {
            if (wd1.day.day < wd2.day.day) {
                return 1;
            } else {
                return -1;
            }
        }
        return 0;
    }

    //метод для определения высокосного года.
    public static boolean isLeap(int year) {
        if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
            return true;
        } else {
            return false;
        }
    }

    //метод для определения кол-вы дней в месяце
    public static int getDays(int month, boolean leap) {
        if (month == 2 && leap == true) {
            return 29;
        } else if (month == 2 && leap == false) {
            return 28;
        } else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            return 31;
        } else if (month == 4 || month == 6 || month == 9 || month == 11) {
            return 30;
        } else {
            throw new IllegalArgumentException("Wrong month");
        }
    }

    //В классе Year должна осуществляться проверка на высокостность (можно реализовать в конструкторе)
    //в результате, установить значение для соотв. атрибута года.
    class Year {
        int year;
        boolean leap;

        Year(int year) {
            if (year >= 1900 && year < 2100) {
                this.year = year;
                leap = isLeap(year);
            } else {
                throw new IllegalArgumentException("Wrong year: " + year + ". Year should be between 1900 and 2100.");
            }
        }
    }

    //В классе Month можно сделать метод который позволит узнать количество дней для того или иного месяца [1-12].
    //Метод можно использовать для подсчета дней в других методах.
    class Month {
        int month;
        int daysQuantity;

        public Month(int month, Year year) {
            if (month >= 1 && month <= 12) {
                this.month = month;
                daysQuantity = getDays(month, year.leap);
            } else {
                throw new IllegalArgumentException("Wrong month: " + month);
            }
        }
    }

    //В классе день мы только проверяем на допустомость этого дня.
    // Зачем так делать непонятно. ООП, хули.
    class Day {
        private int day;

        Day(int day, Month month) {
            if (day >= 1 && day <= month.daysQuantity) {
                this.day = day;
            } else {
                throw new IllegalArgumentException("Wrong day");
            }
        }
    }
}