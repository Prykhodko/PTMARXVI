package HW2;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by TROUBLE on 22.04.16.
 */
public final class StringUtils {
    //функция, которая переворачивает строчку наоборот. Пример: было “Hello world!” стало “!dlrow olleH”
    public static String reverse(String text) {
        if (text.isEmpty()) {
            throw new IllegalArgumentException("type something. i can't reverse nothing");
        } else {
            char[] a = text.toCharArray();
            char[] b = new char[a.length];
            for (int i = 0; i < b.length; i++) {
                b[(a.length-1)-i]= a[i];
            }
            return new String(b);
        }
    }

    //функция, которая определяет является ли строчка полиндромом. Пример: А роза упала на лапу Азора
    public static boolean isPalindrome(String text) {
        if (text.isEmpty()) {
            throw new IllegalArgumentException("type something. null is null, not a palindrome");
        } else {
            String regexp = "[^A-Za-zА-Яа-я1-9]+";
            Pattern pattern = Pattern.compile(regexp);
            Matcher matcher = pattern.matcher(text);
            text = matcher.replaceAll("");
            text = text.toLowerCase();
            boolean bool = false;
            char[] textArray = text.toCharArray();
            for (int i = 0; i < textArray.length/2; i++) {
                if (textArray[i] == textArray[textArray.length-1-i]) {
                    bool = true;
                } else {
                    bool = false;
                    break;
                }
            }
            return bool;
        }
    }

    //функция которая проверяет длину строки, и если ее длина больше 10, то оставить в строке только первые 6 символов, иначе дополнить строку символами 'o' до длины 12.
    public static String checkLength(String text) {
        StringBuilder sb = new StringBuilder(text);
        if (sb.length() > 10) {
            sb = sb.delete(6, sb.length());
        } else {
            while (sb.length()<12) {
                sb.append('o');
            }
        }
        return sb.toString();
    }

    //функция, которая меняет местами первое и последнее слово в строчке
    public static String reverseFirstLastWords(String text) {
        String regularExpression = "(\\b[a-zA-Zа-яА-Я0-9_]+)(.*\\s)(\\b[a-zA-Zа-яА-Я0-9_]+)"; //(\b\w*)(.*\s)(\b\w*)
        Pattern pattern = Pattern.compile(regularExpression);
        Matcher matcher = pattern.matcher(text);
        if (!matcher.find()) {
            throw new IllegalArgumentException("text should contain at least 2 words: "+text);
        } else {
            String newStr = matcher.replaceAll("$3$2$1");
            return newStr;
        }

    }

    //функция, которая меняет местами первое и последнее слово в каждом предложении. (предложения могут разделятся ТОЛЬКО знаком точки)
    public static String reverseFirstLastWordsSentence(String text) {
        String regularExpression = "(\\b[a-zA-Zа-яА-Я0-9_]+)(.*?\\s)(\\b[a-zA-Zа-яА-Я0-9_]+)(\\.)"; //(\b\w*)(.*\s)(\b\w*)
        Pattern pattern = Pattern.compile(regularExpression);
        Matcher matcher = pattern.matcher(text);
        if (!matcher.find()) {
            throw new IllegalArgumentException("text should contain at least 2 words and a dot: "+text);
        } else {
            String newString = matcher.replaceAll("$3$2$1$4");
            return newString;
        }
    }

    //функция, которая проверяет содержит ли строка только символы 'a', 'b', 'c' или нет.
    public static boolean isContainabc (String text) {
        if (text.isEmpty()) {
            throw new IllegalArgumentException("type something. I can't check null");
        } else {
            String regularExpression = "\\A[abc]*\\Z";
            Pattern pattern = Pattern.compile(regularExpression);
            Matcher matcher = pattern.matcher(text);
            return matcher.matches();
        }
    }

    //функция, которая определят является ли строчка датой формата MM.DD.YYYY
    public static boolean isDate(String text) {
        if (text.isEmpty()) {
            throw new IllegalArgumentException("type something. I can't check null");
        } else {
            String regularExpression = "\\A(((0?[123456789])|(1[012]))\\.((3[01])|([12]\\d)|(0?[1-9]))\\.((19|20)\\d\\d))\\Z";
            Pattern pattern = Pattern.compile(regularExpression);
            Matcher matcher = pattern.matcher(text);
            return matcher.matches();
        }
    }

    //функция, которая определяет является ли строчка почтовым адресом
    public static boolean isEmail(String text) {
        if (text.isEmpty()) {
            throw new IllegalArgumentException("type something. I can't check null");
        } else {
            String regularExpression = "\\A[A-Za-z][A-Za-z0-9.-]+@[A-z0-9][A-z0-9-]*\\.[A-z]{2,4}\\Z";
            Pattern pattern = Pattern.compile(regularExpression);
            Matcher matcher = pattern.matcher(text);
            return matcher.matches();
        }
    }

    //функция, которая находит все телефоны в переданном тексте формата +Х(ХХХ)ХХХ-ХХ-ХХ, и возвращает их в виде массива
    public static String[] findPhones(String text) {
        if (text.isEmpty()) {
            throw new IllegalArgumentException("type something. I can't check null");
        } else {
            String regularExpression = "\\b(\\d)(\\d{3})(\\d{3})(\\d{2})(\\d{2})\\b";
            Pattern pattern = Pattern.compile(regularExpression);
            Matcher matcher = pattern.matcher(text);
            List<String> phoneNumbers = new ArrayList<>();
            StringBuilder newSB = new StringBuilder();
            while (matcher.find()) {
                newSB.append("+");
                newSB.append(matcher.group(1));
                newSB.append("(");
                newSB.append(matcher.group(2));
                newSB.append(")");
                newSB.append(matcher.group(3));
                newSB.append("-");
                newSB.append(matcher.group(4));
                newSB.append("-");
                newSB.append(matcher.group(5));
                String newString = newSB.toString();
                newSB.delete(0, newSB.length());
                phoneNumbers.add(newString);
            }
            String[] arrayPhoneNumbers = {};
            arrayPhoneNumbers = phoneNumbers.toArray(new String[phoneNumbers.size()]);
            return arrayPhoneNumbers;
        }



    }
}
