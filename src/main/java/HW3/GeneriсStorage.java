package HW3;

import java.util.Arrays;

/**
 * Created by TROUBLE on 20.04.16.
 */
public class GeneriсStorage<T> {
    T[] elementData;                                        //вроде правильно
    private int lastItemIndex = 0;                          //доступная для записи ячейка массива
    private static final int DEFAULT_SIZE = 10;             //размер массива по умолчанию
    private static final int MAX_ARRAY_SIZE = 2147483639;   //why not?

    //(2.) Создать конструктор с параметром (int size). Size - размер массива.
    public GeneriсStorage(int size) {
        if (size >= 0) {
            elementData = (T[])(new Object[size]);
        } else {
            throw new IllegalArgumentException("Illegal size = "+size);
        }

    }

    //(1.) Создать конструктор без параметров. При вызове такого конструктора должен инициализироваться массив длиной 10.
    public GeneriсStorage() {
        this(DEFAULT_SIZE);
    }

    private boolean checkIndex(int index) {
        if (index >= 0 && index < this.elementData.length) {
            return true;
        } else {
            return false;
        }
    }

    private boolean ensureCapacity() {
        if (lastItemIndex < this.elementData.length) {
            return true;
        } else {
            return false;
        }
    }

    public void growArrow() {
        int var1 = this.elementData.length;
        int var2 = var1 + (var1>>1) + 1;
        if (var2 < MAX_ARRAY_SIZE) {
            T[] clone = (T[])(new Object[var2]);
            System.arraycopy(this.elementData, 0, clone, 0, this.elementData.length);
            elementData = clone;
        }
    }

    //(3.)	Метод add(T obj), который добавит новый элемент в хранилище в конец
    public void add(T obj) {
        if (ensureCapacity()) {
            elementData[lastItemIndex++] = obj;
        } else {
            growArrow();
            elementData[lastItemIndex++] = obj;
        }

    }

    //(4.)	Метод T get(int index), который возвратит элемент по индексу в масиве.
    public T get(int index) {
        if (checkIndex(index)) {
            return elementData[index];
        } else {
            throw new IllegalArgumentException("Illegal index = " + index);
        }
    }

    //(5.)	Метод T[] getAll(), который вернет массив элементов. (Распечатать массив при помощи Arrays.toString(<-Ваш массив->))
    public T[] getAll() {
        return elementData;
    }

    //(6.)	Метод update(int index, T obj), который подменит объект по заданной позиции только, если на этой позиции уже есть элемент.
    public void update(int index, T obj) {
        if (checkIndex(index)){
            if (get(index) != null) {
                elementData[index] = obj;
            }
        } else {
            throw new IllegalArgumentException("Illegal index = "+index);
        }
    }

    //(7.)	Meтод delete(int index), который удалит элемент по индексу и захлопнет пустую ячейку в массиве, если на этой позиции уже есть элемент.
    public void delete(int index) {
        if (checkIndex(index)) {
            if (this.elementData[index]!= null) {
                T[] clone = (T[])(new Object[elementData.length-1]);
                System.arraycopy(this.elementData, 0, clone, 0, index);
                System.arraycopy(this.elementData, index+1, clone, index, clone.length-index);
                elementData = clone;
            }
        } else {
            throw new IllegalArgumentException("Illegal index = " + index);
        }

    }

    //(8.)	Метод delete(T obj), который удалит заданный объект из массива (подходящий под 1 ключ - 3 Ивана)
    public int delete(T obj) {
        int count = 0;
        for (int i = 0; i < this.elementData.length; i++) {
            if (this.elementData[i] == obj) {
                delete(i);
                count++;
            }
        }
        return count;
    }

    //(9.)	Метод который возвратит размер массива ЗАПОЛНЕНОГО
    public int size() {
        return this.elementData.length;
    }

    @Override
    public String toString() {
        return "GeneriсStorage{" +
                "elementData=" + Arrays.toString(elementData) +
                ", lastItemIndex=" + lastItemIndex +
                ", size=" + elementData.length +
                '}';
    }

    //      нужно всегда хранить индекс ячейки массива, который указывает на позицию следующую после последнего элемента.
    //      Изначально это 0 так как массив пуст, после добавления первого элемента индекс указывает на 1 и т.д.
    //      не забывайте что переменная index (get(int index), update(int index, T obj), delete(int index) ) должна всегда
    //      попадать в рамки 0 - индекс последнего элемента.

}
