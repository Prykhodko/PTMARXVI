package HW4.Task4_1;

/**
 * Created by TROUBLE on 11.06.16.
 */
//1.1. Создать класс «автомобиль», в котором определить 2-3 поля.
public class Car {
    String name;
    int speed;

    public Car(String name, int speed) {
        this.name = name;
        this.speed = speed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
