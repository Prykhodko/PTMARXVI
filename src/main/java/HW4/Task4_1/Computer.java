package HW4.Task4_1;

/**
 * Created by TROUBLE on 12.06.16.
 */
//1.2. Создать класс «компьютер», в котором определить 2-3 поля. Класс компьютер должен реализовывать интерфейс Comparable.
public class Computer implements Comparable<Computer>{
    private int gghz;
    private String company;

    public Computer(int gghz, String company) {
        this.gghz = gghz;
        this.company = company;
    }

    public int getGghz() {
        return gghz;
    }

    public void setGghz(int gghz) {
        this.gghz = gghz;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "gghz=" + gghz +
                ", company='" + company + '\'' +
                '}';
    }

    @Override
    public int compareTo(Computer computer) {
        if (company.compareTo(computer.company) > 0) {
            return 1;
        } else if (company.compareTo(computer.company) < 0) {
            return -1;
        } else if (Integer.valueOf(gghz).compareTo(Integer.valueOf(computer.gghz)) > 1) {
            return 1;
        } else if (Integer.valueOf(gghz).compareTo(Integer.valueOf(computer.gghz)) < 1){
            return -1;
        } return 0;
    }
}
