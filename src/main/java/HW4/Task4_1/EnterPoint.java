package HW4.Task4_1;

/**
 * Created by TROUBLE on 11.06.16.
 */
public class EnterPoint {
    public static void main(String[] args) {
        HW4.Task4_1.Car[] carArray = new HW4.Task4_1.Car[3];
        carArray[0] = new HW4.Task4_1.Car("Лада", 150);
        carArray[1] = new HW4.Task4_1.Car("Жигуль", 120);
        carArray[2] = new HW4.Task4_1.Car("Волга", 160);

        HW4.Task4_1.Computer[] computerArray= new HW4.Task4_1.Computer[3];
        computerArray[0] = new HW4.Task4_1.Computer(1660, "Acer");
        computerArray[1] = new HW4.Task4_1.Computer(1800, "HP");
        computerArray[2] = new Computer(3200, "Dell");

        String[] stringArray = new String[4];
        stringArray[0] = "First";
        stringArray[1] = "Second";
        stringArray[2] = "Third";
        stringArray[3] = "Fourth";

        Integer[] integerArray = new Integer[3];
        integerArray[0] = new Integer(10);
        integerArray[1] = new Integer(7);
        integerArray[2] = new Integer(15);

        //System.out.println(EnterPoint.maxOf(carArray));
        System.out.println(EnterPoint.maxOf(computerArray));
        System.out.println(EnterPoint.maxOf(stringArray));
        System.out.println(EnterPoint.maxOf(integerArray));
    }

    //1.3. Создать статический метод, для определения максимального (по compareTo()) значения в массиве объектов.
    // Продемонстрировать работу метода на примере массивов типа Integer, String,
    // «автомобиль» (должна быть ошибка компиляции), «компьютер».
    //public static<T extends Comparable<T>> void getMax(Object[] o) {
    public static <T extends Comparable<T>> T maxOf(T[] ts){
        T max = null;
        for (T t: ts) {
            if (t != null && (max == null || t.compareTo(max) > 0)) {
                max = t;
            }
        }
        return max;
    }

}
