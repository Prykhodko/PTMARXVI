package HW4.Task4_2.Part1;

/**
 * Created by TROUBLE on 19.06.16.
 */
public class Demo {
    public static void main(String[] args) {
        MyDequeImpl<String> stringMyDeque = new MyDequeImpl<>();
        stringMyDeque.addFirst("First line");
        stringMyDeque.addLast("Middle line");
        stringMyDeque.addLast("Last line");
        System.out.println(stringMyDeque.toString());
        System.out.println("~~~~~~~~~~~~`");
        System.out.println(stringMyDeque.getFirst());
        System.out.println(stringMyDeque.getLast());
        System.out.println("~~~~~~~~~~~~`");
        stringMyDeque.removeFirst();
        stringMyDeque.removeLast();
        System.out.println(stringMyDeque.toString());
        System.out.println("~~~~~~~~~~~~`");
        System.out.println(stringMyDeque.contains("Second line"));
        stringMyDeque.addFirst("First line");
        stringMyDeque.addLast("Last line");
        MyDequeImpl<String> myDequeImplTest = new MyDequeImpl<>();
        myDequeImplTest.addFirst("First line");
        myDequeImplTest.addLast("Last line");
        System.out.println(stringMyDeque.containsAll(myDequeImplTest));
    }
}
