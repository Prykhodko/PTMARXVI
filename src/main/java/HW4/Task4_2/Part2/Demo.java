package HW4.Task4_2.Part2;

import java.util.Iterator;

/**
 * Created by TROUBLE on 19.06.16.
 */
public class Demo {
    public static void main(String[] args) {
        MyDeque<Number> deque = new MyDequeImpl<Number>();
        deque.addFirst(433);
        deque.addLast(8.88);


        for (Number element : deque) {
            System.out.println(element);
        }

        Iterator<Number> it = deque.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }


    }
}
