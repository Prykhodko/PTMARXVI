package HW4.Task4_2.Part2;

import java.util.Iterator;

/**
 * Created by TROUBLE on 12.06.16.
 */
public class MyDequeImpl<E> implements HW4.Task4_2.Part2.MyDeque<E> {
    private int size;
    public Node<E> header;

    public MyDequeImpl() {
        header = new Node<>();
        header.element = null;
        header.prev = header.next = header;
        size = 0;
    }

    @Override
    public void addFirst(E e) {
        Node<E> newNode = new Node<>(e, header.next, header);
        newNode.prev.next = newNode;
        newNode.next.prev = newNode;
        size++;
    }
    @Override
    public void addLast(E e) {
        Node<E> newNode = new Node<E>(e, header, header.prev);
        newNode.prev.next = newNode;
        newNode.next.prev = newNode;
        size++;
    }

    @Override
    public E removeFirst() {
        if (size() == 0) {
            throw new IndexOutOfBoundsException("There are no elements!");
        } else {
            Node<E> removed = header.next;
            header.next = removed.next;
            removed.next.prev = removed.prev;
            removed.next = removed.prev = null;
            E returned = removed.element;
            removed.element = null;
            size--;
            return returned;
        }
    }

    @Override
    public E removeLast() {
        if (size() == 0) {
            throw new IndexOutOfBoundsException("There are no elements!");
        } else {
            Node<E> removed = header.prev;
            removed.prev.next = removed.next;
            header.prev = removed.prev;
            removed.next = removed.prev = null;
            E returned = removed.element;
            removed.element = null;
            size--;
            return returned;
        }
    }

    private E unlink(Node<E> unlinkedNode) {
        if (size() == 0) {
            throw new IndexOutOfBoundsException("There are no elements!");
        } else {
            unlinkedNode.prev.next = unlinkedNode.next;
            unlinkedNode.next.prev = unlinkedNode.prev;
            E returned = unlinkedNode.element;
            unlinkedNode.prev = unlinkedNode.next = null;
            unlinkedNode.element = null;
            size--;
            return returned;
        }
    }

    @Override
    public E getFirst() {
        return header.next.element;
    }

    @Override
    public E getLast() {
        return header.prev.element;
    }

    @Override
    public boolean contains(Object o) {
        if (size == 0) {
            return false;
        }
        E e = (E) o;
        Node<E> checked = header.next;

        for (int i = 0; i < size; i++, checked = checked.next) {
            if (e.equals(checked.element)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void clear() {
        Node<E> checked = header.next.next;
        for (Node<E> x = header; x!=null; ) {
            Node<E> next = x.next;
            x.element = null;
            x.next = null;
            x.prev = null;
            x = next;
        }
        header.next = header;
        header.prev = header;
        size = 0;
    }

    @Override
    public Object[] toArray() {
        Object[] result = new Object[size];
        int i = 0;
        for (Node<E> x = header.next; x != header; x = x.next) {
            result[i++] = x.element;
        }
        return result;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean containsAll(HW4.Task4_2.Part2.MyDeque<? extends E> deque) {
        if (deque.equals(this)) {
            return true;
        } else if (deque.size() != 0 && this.size != 0) {
            Object[] array = deque.toArray();
            int on = 0;
            for (int i = 0; i < array.length; i++) {
                for (Node<E> x = header.next; x != header; x = x.next) {
                    if (((E)array[i]).equals(x.element)) {
                        on++;
                        break;
                    }
                }
            }
            return on == array.length? true : false;
        } else {
            return false;
        }

    }

    private static class Node<E> {
        // хранимый элемент
        E element;
        // ссылка на следующий элемент списка
        Node<E> next;
        // ссылка на предыдущий элемент списка
        Node<E> prev;

        Node(E element, Node<E> next, Node<E> prev) {
            this.element = element;
            this.next = next;
            this.prev = prev;
        }

        Node() {
        }

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("MyDequeImpl{");
        for (Node<E> x = header.next; x != header; x = x.next) {
            sb.append(x.element.toString());
            sb.append('\n');
        }
        sb.append('}');
        return sb.toString();
    }

    @Override
    public Iterator<E> iterator() {
        return new IteratorImpl();
    }

    private class IteratorImpl implements Iterator<E> {
        private Node<E> current = header;
        private int checker = 0;
        // проверяет, есть ли следующий элемент для выборки методом next
        @Override
        public boolean hasNext() {
            if (current.next!=header) {
                checker = 0;
                return true;
            } else {
                checker = -1;
                return false;
            }
        }

        // возвращает следующий элемент
        @Override
        public E next() {
            if (checker != 0) {
                throw new IllegalStateException("call hasNext() before;");
            }
            current = current.next;
            checker = 1;
            return current.element;
        }

        // удаляет элемент, который был возвращен ранее методом next
        //ЕСЛИ ПЕРЕД ВЫЗОВОМ remove НЕ БЫЛ ВЫЗВАН МЕТОД next
        // ИЛИ ПЕРЕД ВЫЗОВОМ remove БЫЛ ВЫЗВАН remove (повторный вызов remove)
        // ВЫБРОСИТЬ ИСКЛЮЧЕНИЕ (так и вставить в код):
        @Override
        public void remove() {
            if (checker != 1) {
                throw new IllegalStateException("Если перед вызовом remove не был вызван next или перед вызовом remove был вызван remove.");
            } else {
                unlink(current.prev);
                checker = -1;
            }
        }
    }
}
