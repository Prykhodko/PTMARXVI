package HW4.Task4_2.Part3;

/**
 * Created by TROUBLE on 19.06.16.
 */
public class Demo {
    public static void main(String[] args) {

        MyDeque<Number> deque = new MyDequeImpl<Number>();
        deque.addFirst(433);
        deque.addLast(8.88);


        ListIterator<Number> listIt = deque.listIterator();
        while (listIt.hasNext())
            System.out.println(listIt.next());
        while (listIt.hasPrevious())
            System.out.println(listIt.previous());

    }
}
