package HW4.Task4_2.Part3;

import java.util.Iterator;

/**
 * Created by TROUBLE on 20.06.16.
 */
public interface ListIterator<E> extends Iterator<E> { //java.
    // проверяет, есть ли предыдущий элемент для выборки методом previous
    boolean hasPrevious();
    // возвращает предыдущий элемент
    E previous();
    // заменяет элемент, который на предыдущем шаге был возвращен next/previous на данный элемент
    void set(E e);
    // удаляет элемент, который на предыдущем шаге был возвращен next/previous
    void remove();
    //Методы set/remove могут быть вызваны только после next/previous.
    //Повторный вызов (подряд) set/remove должен приводить к выбросу исключения IllegalStateException
}
