package HW5;

import java.io.*;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by TROUBLE on 31.05.16.
 */
public class EnterPoint {
    public static void main(String[] args) {
        Console console = System.console();
        if (console != null) {
            while (true) {
                //Просим у пользователя ввести путь к файлу
                String path = console.readLine("Приложение переводчик. Введите путь к файлу, который необходимо перевести или введите exit для выхода из приложения\n");
                if (path.equals("exit")) {
                    //если пользователь введет exit он выйдет из приложения
                    System.out.println("Выход из приложения");
                    break;
                } else if (!isAppropriateFile(path)){
                    //если введенный путь некорректен выведет это предупреждение
                    System.out.println("Введите допустимый абсолютный путь к файлу, который может быть прочитан");
                } else {
                    //в случае если путь корректен идем дальше, т.е. узнаем направление перевода
                    StringBuilder entryText = new StringBuilder("Допустимые направления перевода:");
                    for (TranslationDirection td: TranslationDirection.values()) {
                        entryText.append('\n');
                        entryText.append(td.commandCall + " - " + td.usersCall);
                    }
                    System.out.println(entryText.toString());
                    while (true) {
                        String directionTranslation = console.readLine("Введите числовой номер! направления перевода или введите exit для выхода из приложения\n");
                        TranslationDirection translationDirection = null;
                        if (directionTranslation.equals("exit")) {
                            //если пользователь введет exit он выйдет из приложения
                            System.out.println("Выход из приложения");
                            break;
                        } else if (!checkTranslateDirection(directionTranslation)) {
                            System.out.println("Такого направления перевода не существует");
                        } else {
                            translationDirection = getTranslationDirection(directionTranslation);
                            HashMap<String, String> dictionary = getDictionary(translationDirection);
                            String result = translateFile(path, dictionary);
                            System.out.println(result);
                            break;
                        }
                        break;
                    }
                }
            }
        }
    }

    private static boolean checkTranslateDirection(String translationDirection) {
        //определим енум направление перевода
        //в цикле проверим введенное значение перевода с возможными значениями и определим енум
        for (TranslationDirection td : TranslationDirection.values()) {
            if (td.commandCall == Integer.valueOf(translationDirection)) {
                return true;
            }
        }
        return false;
    }

    private static TranslationDirection getTranslationDirection(String translationDirection) {
        for (TranslationDirection td : TranslationDirection.values()) {
            if (td.commandCall == Integer.valueOf(translationDirection)) {
                return td;
            }
        }
        return null;
    }

    private static HashMap<String, String> getDictionary(TranslationDirection translationDirection) {
        //определяем путь к словарю в зависимости от направления перевода
        String path = translationDirection.path;
        //выгружаем текст из этого файла
        StringBuilder stringBuilder = null;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(path),"windows-1251"));) {
            stringBuilder = new StringBuilder();
            String string;
            while ((string = bufferedReader.readLine()) != null) {
                stringBuilder.append(string);
                stringBuilder.append("\n");
            }
            string = stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //
        String beforeRegex = stringBuilder.toString();
        //создаем хэшмапу, куда будем складывать переводы
        HashMap<String, String> dictionary = new HashMap<String, String>();
        //работа с паттернами и загрузка слов в хэшмапу
        String regularExpr = "([A-Za-zА-Яа-я-'()]*);-;([A-Za-zА-Яа-я-'()]*)";
        Pattern pattern = Pattern.compile(regularExpr);
        Matcher matcher = pattern.matcher(beforeRegex);
        while (matcher.find()) {
            dictionary.put(matcher.group(1), matcher.group(2));
        }
        return dictionary;
    }

    private static boolean isAppropriateFile(String path) {
        File file = new File(path);
        if (!file.exists()) return false;
        if (!file.isFile()) return false;
        if (!file.canRead()) return false;
        return true;
    }

    private static String translateFile(String path, HashMap<String, String> dictionary) {
        StringBuilder sb = null;
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            sb = new StringBuilder();
            String l;
            while ((l = br.readLine()) != null) {
                sb.append(l);
                sb.append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        String regularExpr = "([а-яА-Яa-zA-Z-']+)([\\s,.!?]*)";
        Pattern pattern = Pattern.compile(regularExpr);
        Matcher matcher = pattern.matcher(sb);

        StringBuilder newOne = new StringBuilder();

        while (matcher.find()) {
            if (dictionary.containsKey(matcher.group(1))) {
                newOne.append(dictionary.get(matcher.group(1)));
                newOne.append(matcher.group(2));
            } else {
                newOne.append(matcher.group(1));
                newOne.append(matcher.group(2));
            }
        }
        return newOne.toString();
    }
}
