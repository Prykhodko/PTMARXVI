package HW5;

/**
 * Created by TROUBLE on 31.05.16.
 */
public enum TranslationDirection {
    RU_EN("D:\\RU-EN.txt", "русско-английский", 1), EN_RU("D:\\EN-RU.txt", "англо-русский", 2);

    String path;
    String usersCall;
    int commandCall;
    TranslationDirection(String path, String usersCall, int commandCall) {
        this.path = path;
        this.usersCall = usersCall;
        this.commandCall = commandCall;
    }

    @Override
    public String toString() {
        return "TranslationDirection{" +
                "path='" + path + '\'' +
                ", usersCall='" + usersCall + '\'' +
                ", commandCall=" + commandCall +
                '}';
    }
}
