package HW6;

import java.util.List;

/**
 * Created by TROUBLE on 25.05.16.
 */
public class EnterPoint {
    public static void main(String[] args) {
        //добавляем книгу в конец
        MyIOStream.addBook("D:\\Book.txt", new HW6.Book("Моя жизнь", "Приходько Никита", 2016));
        //читаем все книги в список
        List<HW6.Book> myBookList = MyIOStream.readBooksFromFile("D:\\Book.txt");
        System.out.println(myBookList.toString());

    }
}
