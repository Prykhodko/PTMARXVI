package HW6;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by TROUBLE on 25.05.16.
 */
public class MyIOStream {
    //Необходимо создать приложение которое прочитает файл и заполнит даными из него, колекцию обьектов Book.
    public static List<HW6.Book> readBooksFromFile(String path) {
        StringBuilder stringBuilder = null;
        //чтение с файла
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(path),"windows-1251"));) {
            stringBuilder = new StringBuilder();
            String string;
            while ((string = bufferedReader.readLine()) != null) {
                stringBuilder.append(string);
                stringBuilder.append("\n");
            }
            string = stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String beforeRegex = stringBuilder.toString();
        List<HW6.Book> bookList = new ArrayList<>();

        String regularExpr = "\\n{0,1}(\\W*);(\\W*\\d*);(-{0,1}\\d{1,4})";
        Pattern pattern = Pattern.compile(regularExpr);
        Matcher matcher = pattern.matcher(beforeRegex);
        while (matcher.find()) {
            bookList.add(new HW6.Book(matcher.group(2), matcher.group(1), Integer.valueOf(matcher.group(3))));
        }
        return bookList;

    }

    //*В дополнение реализовать метод для добавления обьекта Book в файл. **Возможно использовать сериализацию.
    public static void addBook(String path, HW6.Book book) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(book.getAuthor()).append(';').append(book.getTitle()).append(';').append(book.getYear());
        String temp = stringBuilder.toString();
        //System.out.println(temp);
        try (BufferedWriter br = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path, true), "windows-1251"));){
            br.newLine();
            br.write(temp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
