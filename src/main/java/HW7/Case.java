package HW7;

/**
 * Created by TROUBLE on 11.06.16.
 */
public class Case {
    String content;             //что внутри

    public Case(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public synchronized void takeCase(DrugDealer drugDealer) {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        drugDealer.getSuitcase().getCase();
    }

    public synchronized void getCase() {
        System.out.println("Запрос на обмен");
    }
}
