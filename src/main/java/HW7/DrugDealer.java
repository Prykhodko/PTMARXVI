package HW7;

/**
 * Created by TROUBLE on 11.06.16.
 */
public class DrugDealer extends Thread {
    DrugDealerType type;        //тип дилера
    String name;                //имя
    HW7.Case suitcase;              //чемодан с...
    DrugDealer contrAgent;      //с кем торгует

    public DrugDealer(DrugDealerType type, String name, HW7.Case suitcase) {
        this.type = type;
        this.name = name;
        this.suitcase = suitcase;
    }

    public HW7.Case getSuitcase() {
        return suitcase;
    }

    public void setSuitcase(HW7.Case suitcase) {
        this.suitcase = suitcase;
    }

    public void setContrAgent(DrugDealer contrAgent) {
        this.contrAgent = contrAgent;
    }

    @Override
    public void run() {
        suitcase.takeCase(contrAgent);
    }
};

