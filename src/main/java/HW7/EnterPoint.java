package HW7;

/**
 * Created by TROUBLE on 11.06.16.
 */
public class EnterPoint {
    public static void main(String[] args) {
        //1. Создать поток, который печатал бы текущее время каждую секунду.
        //Сделать это двумя способами - при помощи расширения класса Thread
        //и при помощи реализации интерфейса Runnable.
        // Предусмотреть возможность его завершения путем ввода любого символа с клавиатуры.
/*        Thread t1 = new ScannerWaiter();
        Thread t2 = new ThreadExt();
        t2.setDaemon(true);
        t1.start();
        t2.start();*/

/*        //2. Разработать пример взаимной блокировки двух потоков.
        DrugDealer stan = new DrugDealer(DrugDealerType.SELLER, "Stan", new Case("Cocaine"));
        DrugDealer bob = new DrugDealer(DrugDealerType.BUYER, "Bob", new Case("MillionDollars"));
        stan.setContrAgent(bob);
        bob.setContrAgent(stan);
        stan.start();
        bob.start();*/

        TwoCounter tc = new TwoCounter();
        EqualAndRise ear1 = new EqualAndRise(tc);
        EqualAndRise ear2 = new EqualAndRise(tc);

        ear1.start();
        ear2.start();

    }
}
