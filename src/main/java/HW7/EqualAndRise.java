package HW7;

/**
 * Created by TROUBLE on 11.06.16.
 */
public class EqualAndRise extends Thread{

    HW7.TwoCounter twoCounter;

    EqualAndRise(HW7.TwoCounter twoCounter) {
        this.twoCounter = twoCounter;
    }

    synchronized int compare() {
        if (twoCounter.counter1 > twoCounter.counter2) return 1;
        if (twoCounter.counter2 > twoCounter.counter1) return -1;
        return 0;
    }

    @Override
    public void run() {
        while (true) {
            System.out.println(compare());
            twoCounter.counter1++;
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            twoCounter.counter2++;
        }
    }
}
