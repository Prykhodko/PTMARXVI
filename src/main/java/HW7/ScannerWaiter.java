package HW7;

import java.util.Scanner;

/**
 * Created by TROUBLE on 10.06.16.
 */
public class ScannerWaiter extends Thread {
    Scanner sc = new Scanner(System.in);

    @Override
    public void run() {
        if (!sc.hasNext()) {
            ScannerWaiter.interrupted();
        }
    }
}
