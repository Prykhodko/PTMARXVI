package HW7;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * Created by TROUBLE on 10.06.16.
 */
public class ThreadExt extends Thread {
    Scanner sc = new Scanner(System.in);

    @Override
    public void run() {
        printCurrentDateTime();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void printCurrentDateTime () {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
    }


}
