package Practice1;

/**
 * Created by TROUBLE on 14.05.16.
 */
public class ArrayProd {
    public static int prod (int [] Array) {
        int prod = 1;
        for (int i: Array) prod *= i;
        return prod;
    }
}
