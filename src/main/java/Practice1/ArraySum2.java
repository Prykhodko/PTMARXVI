package Practice1;

/**
 * Created by TROUBLE on 14.05.16.
 */
public class ArraySum2 {

    private int[] Array;

    ArraySum2 (int[] Array) {
        this.Array = Array;
    }

    public int sum () {
        int sum = 0;
        for (int i: Array) sum += i;
        return sum;
    }
}
