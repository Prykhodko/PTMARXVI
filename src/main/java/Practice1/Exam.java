package Practice1;

/**
 * Created by TROUBLE on 14.05.16.
 */
public class Exam {
    Lecture lecture;
    int year;
    int mark;
    Semester semester;

    Exam(Lecture lecture, int year, int mark, Semester semester) {
        //12 бальная система, год должен быть от 2000 до 2020
        if ((mark > 1 && mark < 13) && (year > 2000 && year < 2020)) { //if ((mark > 1 && mark < 13) && (year > 2000 && year < 2020)) {
            this.lecture = lecture;
            this.mark = mark;
            this.year = year;
            this.semester = semester;
        } else {
            throw new IllegalArgumentException("Your arguments are wrong. Mark had to be more or equal than 1 and less than or equal to 13: " + mark +
                    '\n' + "Year had to been between 2000 and 2020. " + year);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Exam exam = (Exam) o;

        if (year != exam.year) return false;
        if (lecture != exam.lecture) return false;
        return semester == exam.semester;

    }

    @Override
    public int hashCode() {
        int result = lecture.hashCode();
        result = 31 * result + year;
        result = 31 * result + semester.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return lecture.toString() + ", в " + year + " году, " + semester + " семестр, отметка - " + mark + '\n';
    }
}
