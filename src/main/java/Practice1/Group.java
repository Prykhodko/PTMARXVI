package Practice1;

/**
 * Created by TROUBLE on 14.05.16.
 */
public class Group {
    int course;
    Faculty faculty;

    Group(int course, Faculty faculty) {
        if (course >= 1 || course <= 6) {
            this.course = course;
            this.faculty = faculty;
        } else {
            throw new IllegalArgumentException("Wrong curse: " + course);
        }
    }

    @Override
    public String toString() {
        return faculty + "-" + course;
    }
}
