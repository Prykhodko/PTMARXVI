package Practice1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TROUBLE on 14.05.16.
 */
public class Student {
    private String firstName;
    private String lastName;
    private Practice1.Group studentsGroup;
    public ArrayList<Practice1.Exam> examList;

    Student(String firstName, String lastName, Practice1.Group studentsGroup, ArrayList<Practice1.Exam> examList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.studentsGroup = studentsGroup;
        this.examList = examList;
    }

    Student(String firstName, String lastName, Practice1.Group studentsGroup) {
        this(firstName, lastName, studentsGroup, new ArrayList<>());
    }

    //узнать наивысшую оценку среди всех экзаменов по данному предмету
    //предполагал что в параметр передается группа студентов, по которым необходимо это высчитать, например, группа
    public static int maxMark(Lecture lecture, List<Student> studentList) {
        int maxMark = 0;
        for (int i = 0; i < studentList.size(); i++) {
            for (int j = 0; j < studentList.get(i).examList.size(); j++) {
                if (studentList.get(i).examList.get(j).lecture == lecture && studentList.get(i).examList.get(j).mark > maxMark) {
                    maxMark = studentList.get(i).examList.get(j).mark;
                }
            }
        }
        return maxMark;
    }

    //добавить оценку по экзамену
    public void addExamMark(Practice1.Exam exam) {
        if (!examList.contains(exam)) {
            examList.add(exam);
        } else {
            throw new IllegalArgumentException("List actually contains this Exam");
        }

    }

    //удалить оценку по экзамену
    //если он такой экзамен не сдавал - сгенерировать исключение!
    public void deleteExamMark(Exam exam) {
        if (examList.contains(exam)) {
            examList.remove(exam);
        } else {
            throw new IllegalArgumentException("This student didn't pass exam");
        }
    }

    //узнать число экзаменов, которые он сдал с указанной оценкой
    public int examQuantity(int mark) {
        int examQuantity = 0;
        for (int i = 0; i < examList.size(); i++) {
            if (examList.get(i).mark == mark) {
                examQuantity++;
            }
        }
        return examQuantity;
    }

    //узнать его средний балл за указанный семестр;
    public double averageMarkForSemester(int year, Semester semester) {
        int examQuantity = 0;
        int sumMark = 0;
        for (int i = 0; i < examList.size(); i++) {
            if (examList.get(i).year == year && examList.get(i).semester == semester) {
                examQuantity++;
                sumMark += examList.get(i).mark;
            }
        }
        if (examQuantity != 0) {
            return Math.round((double)sumMark/examQuantity * 100)/100.0;
        }
        return 0.0;
    }

    @Override
    public String toString() {
        return lastName + ", " + firstName + " (" + studentsGroup + ") " + '\n' +
                ", examList=" + examList;
    }
}
