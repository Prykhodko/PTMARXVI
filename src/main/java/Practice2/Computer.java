package Practice2;

/**
 * Created by TROUBLE on 13.04.16.
 */
public class Computer {
    //private fields
    private String model;
    private String description;
    private String guarantee;
    private int productCode;
    private int price;
    private String availability;

    //constructor
    Computer(String model, String description, String guarantee, int productCode, int price, String availability) {
        this.model = model;
        this.description = description;
        this.guarantee = guarantee;
        this.productCode = productCode;
        this.price = price;
        this.availability = availability;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "model='" + model + '\'' +
                ", description='" + description + '\'' +
                ", guarantee='" + guarantee + '\'' +
                ", productCode=" + productCode +
                ", price=" + price +
                ", availability='" + availability + '\'' +
                '}' + "\n";
    }
}
