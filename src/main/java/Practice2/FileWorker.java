package Practice2;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by TROUBLE on 12.04.16.
 */
public class FileWorker {

    private static String read(String filePath) {
        File file = new File(filePath);
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file.getAbsoluteFile()));

            try {
                String s;
                while ((s = br.readLine()) != null) {
                    sb.append(s);
                    sb.append("\n");
                }
            } finally {
                br.close();
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return sb.toString();

    }

    private static String encodingRead(String filePath, String encoding) { //windows­1251
        //File file = new File(filePath);
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath),encoding));
            String s;
            while ((s = br.readLine()) != null) {
                sb.append(s).append(System.getProperty("line.separator"));
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }

    static void firstTask(String filePath) {
        String superText = read(filePath);
        String regularExpr = "<td>(\\d{1,4})<\\/td><td>(.*?)<\\/td><td>(.*?)<\\/td>";
        Pattern pattern = Pattern.compile(regularExpr);
        Matcher matcher = pattern.matcher(superText);
        List<Practice2.baby2008> b2 = new ArrayList<>();
        while (matcher.find()) {
            Integer r = Integer.valueOf(matcher.group(1));
            b2.add(new Practice2.baby2008(r, matcher.group(2), matcher.group(3)));
        }                                                           //конец
        System.out.println(b2);
    }

    static void secondTask(String path, String encoding) {
        String regularExpr2 =
                "class=\"item_name\".*?title=\"(.*?)\">" + ".*" +                                       //модель
                        "<p class=\"description\">(.*?\\s*.*?)<br \\/>" + ".*" +                        //описание
                        "Гарантия:<\\/a>\\s*(.*?)<\\/p>" + ".*\\s*.*\\s*.*\\s*.*\\s*.*\\s*" +           //гарантия
                        "Код товара: (.*?)<\\/span>" + ".*?" +                                          //код товара
                        "<span class=\"price cost\">(\\d{4,5}).*?<\\/span>" + ".*?" +                   //цена
                        "<p class=\"nal.*?\">(.*?)<\\/p>";                                              //есть в наличии

        String superText = FileWorker.encodingRead(path,encoding);
        Pattern pattern = Pattern.compile(regularExpr2);             //скомпилированое регулярное выражение
        Matcher matcher = pattern.matcher(superText);               //хранит результат обработки текста регулярным выражением
        List <Practice2.Computer> computers = new ArrayList<>();

        while (matcher.find()) {
            int tempCode = Integer.valueOf(matcher.group(4));
            int tempPrice = Integer.valueOf(matcher.group(5));
            computers.add(new Computer(matcher.group(1), matcher.group(2), matcher.group(3), tempCode, tempPrice, matcher.group(6)));
        }
        System.out.println(computers);
    }
}
