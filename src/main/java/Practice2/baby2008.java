package Practice2;

/**
 * Created by TROUBLE on 13.04.16.
 */
public class baby2008 {
    private int rate;
    private String men;
    private String women;

    baby2008(int r, String m, String w) {
        this.rate = r;
        this.men = m;
        this.women = w;
    }

    @Override
    public String toString() {
        return "baby2008{" +
                "rate=" + rate +
                ", men='" + men + '\'' +
                ", women='" + women + '\'' +
                '}' + "\n";
    }
}
