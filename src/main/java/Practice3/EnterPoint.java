package Practice3;

import Practice3.InMemoryDatabase.UserCRUDImplementation;
import Practice3.Model.Book;
import Practice3.Model.Report;
import Practice3.Model.User;

import java.util.Date;

/**
 * Created by TROUBLE on 17.07.16.
 */
public class EnterPoint {
    public static void main(String[] args) {
        User test1 = new User("tre", "tre-tre", "123", new Date(2015, 04, 06));
        User test2 = new User("rew", "rew-rew", "123", new Date(2015, 04, 06));

        Book book1 = new Book("Ger", "ger123", 10);

        Report report = new Report(book1, test1, new Date(2015, 04, 06), null);

        UserCRUDImplementation userCRUDImplementation = new UserCRUDImplementation();

    }
}
