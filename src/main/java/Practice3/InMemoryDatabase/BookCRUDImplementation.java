package Practice3.InMemoryDatabase;

import Practice3.Model.Book;

/**
 * Created by TROUBLE on 17.07.16.
 */
public class BookCRUDImplementation implements CRUD<Long, Book> {
    @Override
    public Long getKey(Book entity) {
        return null;
    }

    @Override
    public Book getByKey(Long key) {
        return null;
    }

    @Override
    public void save(Book entity) {

    }

    @Override
    public void delete(Long key) {

    }

    @Override
    public void update(Book entity) {

    }
}
