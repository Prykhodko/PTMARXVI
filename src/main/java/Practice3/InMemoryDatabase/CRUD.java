package Practice3.InMemoryDatabase;

/**
 * Created by TROUBLE on 17.07.16.
 */
public interface CRUD <K, V> {
    K getKey(V entity);
    V getByKey(K key);
    void save(V entity);
    void delete(K key);
    void update(V entity);
}
