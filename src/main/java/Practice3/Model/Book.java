package Practice3.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TROUBLE on 17.07.16.
 */
public class Book {
    long id;
    String author;
    String title;
    int count;
    List<User> users;
    List<Report> reports;

    public Book(String author, String title, int count) {
        this.author = author;
        this.title = title;
        this.count = count;
        users = new ArrayList<>();
        reports = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;

        Book book = (Book) o;

        if (count != book.count) return false;
        if (!author.equals(book.author)) return false;
        return title.equals(book.title);

    }

    @Override
    public int hashCode() {
        int result = author.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + count;
        return result;
    }
}
