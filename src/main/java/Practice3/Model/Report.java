package Practice3.Model;

import java.util.Date;

/**
 * Created by TROUBLE on 17.07.16.
 */
public class Report {
    long id;
    Book book;
    User user;
    Date rent;
    Date returns;

    public Report(Book book, User user, Date rent, Date returns) {
        this.book = book;
        this.user = user;
        this.rent = rent;
        this.returns = returns;
        book.reports.add(this);
        user.books.add(book);
    }


}
