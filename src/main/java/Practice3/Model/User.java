package Practice3.Model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by TROUBLE on 17.07.16.
 */
public class User {
    long id;
    String name;
    String login;
    String password;
    Date birthday;
    List<Book> books;

    public User(String name, String login, String password, Date birthday) {
        this.name = name;
        this.login = login;
        this.password = password;
        this.birthday = birthday;
        books = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (!name.equals(user.name)) return false;
        if (!login.equals(user.login)) return false;
        if (!password.equals(user.password)) return false;
        return birthday.equals(user.birthday);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + login.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + birthday.hashCode();
        return result;
    }
}
