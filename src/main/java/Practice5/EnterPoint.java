package Practice5;

import java.io.*;

/**
 * Created by TROUBLE on 22.05.16.
 */
public class EnterPoint {
    public static void main(String[] args) {
        Practice5.MyIOUtils.writeFileRandomInt("D:\\test.txt", 100);
        Practice5.MyIOUtils.sortFileRandomInt("D:\\test.txt");
        Practice5.MyIOUtils.printStudentAverageMark("D:\\student.txt");

        long startWithBuf = System.currentTimeMillis();
        Practice5.MyIOUtils.copyBufferIO("E:\\Velikiy_diktator(DVDRip.Rus.XviD).by_frolovdd.avi", "E:\\copyBuf.avi");
        long endWithBuf = System.currentTimeMillis();
        long deltaWithBuf = endWithBuf - startWithBuf;


        long startWithoutBuf = System.currentTimeMillis();
        Practice5.MyIOUtils.copyNotBufferIO("E:\\Velikiy_diktator(DVDRip.Rus.XviD).by_frolovdd.avi", "E:\\copyNotBuf.avi");
        long endWithoutBuf = System.currentTimeMillis();
        long deltaWithoutBuf = endWithoutBuf - startWithoutBuf;

        System.out.println("время копирования с буфером в мс: " + deltaWithBuf);
        System.out.println("время копирования без буфера в мс: " + deltaWithoutBuf);

        //6. Создать класс Student и класс Group содержащий коллекцию студентов.
        //Выполнить сериализацию и десериализацию объекта класса Group.

        Practice5.Student ivanov = new Practice5.Student("Ivan", "Ivanov");
        Practice5.Student petrov = new Practice5.Student("Andrei", "Petrov");
        Practice5.Student sidorov = new Practice5.Student("Zohan", "Sidorov");
        Practice5.Student kozlov = new Student("Oleg", "Kozlov");

        StudentGroup MEO5 = new StudentGroup("MEO", 5);

        MEO5.addStudent(ivanov);
        MEO5.addStudent(petrov);
        MEO5.addStudent(sidorov);
        MEO5.addStudent(kozlov);

        //сериализация
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("GroupData"));){
            oos.writeObject(MEO5);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //десериализация с кастованием
        StudentGroup unknown = null;

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("GroupData"));){
            unknown = (StudentGroup) ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println(unknown);
    }
}
