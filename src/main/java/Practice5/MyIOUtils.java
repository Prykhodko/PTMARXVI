package Practice5;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by TROUBLE on 22.05.16.
 */
public class MyIOUtils {
    //1. Создать и заполнить файл случайными целыми числами.
    public static void writeFileRandomInt(String path, int quantity) {
        Random random = new Random();
        //System.out.println(Arrays.toString(array));
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path)));) {
            for (int i = 0; i < quantity; i++) {
                bw.write(String.valueOf(random.nextInt(100000)));
                bw.write(";");
            }
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //2. Создать метод который отсортирует содержимое файла из пункта 1 по возрастанию
    public static void sortFileRandomInt(String path) {
        int temp;
        List<Integer> integerList = new ArrayList<>();
        try (Scanner tokenizingScanner = new Scanner(new FileReader(path));){
            tokenizingScanner.useDelimiter(";");
            while (tokenizingScanner.hasNext()) {
                temp = tokenizingScanner.nextInt();
                integerList.add(temp);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Collections.sort(integerList);
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path)));) {

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //3. В файле построчно содержится список студентов и их оценки. Вывести на экран фамилии студентов,
    // которые имеют средний балл более "90". Пример содержимого файла:
    // Illia Vinnichenko = 90
    // Koliy Lototskiy = 84
    public static void printStudentAverageMark(String path) {
        StringBuilder stringBuilder = null;
        //чтение с файла
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path));) {
            stringBuilder = new StringBuilder();
            String string;
            while ((string = bufferedReader.readLine()) != null) {
                stringBuilder.append(string);
                stringBuilder.append("\n");
            }
            string = stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //итог после чтения
        String beforeRegex = stringBuilder.toString();
        List<Practice5.Student> studentList = new ArrayList<>();

        String regularExpr = "(\\w*) (\\w*) = (\\d{1,2})";
        Pattern pattern = Pattern.compile(regularExpr);
        Matcher matcher = pattern.matcher(beforeRegex);
        while (matcher.find()) {
            //временный студент, которы удобно оперировать, сравнивать и т.д.
            Practice5.Student temp = new Practice5.Student(matcher.group(1), matcher.group(2));
            //проверка на наличие с списке студентов.
            //если студент присутствует, мы добавляем ему оценку
            if (studentList.contains(temp)) {
                studentList.get(studentList.indexOf(temp)).addMark(Integer.parseInt(matcher.group(3)));
            //если студента нет, мы сначала добавляем студента, затем добавляем ему оценку
            } else {
                studentList.add(temp);
                studentList.get(studentList.indexOf(temp)).addMark(Integer.parseInt(matcher.group(3)));
            }
        }

        //вывод на экран через вспомогательный список. Можно, конечно, и без него, но так некрасиво.
        List<String> studentAverageMark = new ArrayList<>();
        for (int i = 0; i < studentList.size(); i++) {
            if (studentList.get(i).getAverageMark() > 90.0) {
                studentAverageMark.add(studentList.get(i).getSecondName());
            }
        }
        System.out.println(studentAverageMark.toString());
    }

    //4. Скопировать содержимое одного файла в другой файл с использованием
    //буферизированных и не буферизированных потоков. Сравнить время работы.
    public static void copyBufferIO(String pathFrom, String pathWhere) {
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(pathFrom));
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(pathWhere));) {
            byte [] buffer = new byte[1024*4];
            int length;
            while ((length = bis.read(buffer)) > 0) {
                bos.write(buffer, 0 , length);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void copyNotBufferIO(String pathFrom, String pathWhere) {
        try (FileInputStream fis = new FileInputStream(pathFrom); FileOutputStream fos = new FileOutputStream(pathWhere)) {
            byte [] buffer = new byte[1024*4];
            int length;
            while ((length = fis.read(buffer)) > 0) {
                fos.write(buffer, 0 , length);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
