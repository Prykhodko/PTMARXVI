package Practice5;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by TROUBLE on 22.05.16.
 */
public class Student implements Serializable{
    private String firstName;
    private String secondName;
    private List<Integer> markList;

    Student(String firstName, String secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
        markList = new ArrayList<>();
    }

    void addMark(int mark) {
        markList.add(Integer.valueOf(mark));
    }

    public double getAverageMark() {
        int markCounter = 0;
        int markSum = 0;
        for (Integer i: markList) {
            markSum += i;
            markCounter++;
        }
        if (markSum == 0) {
            return 0.0;
        } else {
            return (double)markSum/markCounter;
        }
    }

    public String getSecondName() {
        return secondName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;

        Student student = (Student) o;

        if (!firstName.equals(student.firstName)) return false;
        return secondName.equals(student.secondName);

    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + secondName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", markList=" + markList +
                '}';
    }
}
