package Practice5;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by TROUBLE on 22.05.16.
 */
public class StudentGroup implements Serializable {

    private String faculty;
    private int number;
    private List<Practice5.Student> studentList = null;

    public StudentGroup(String faculty, int number) {
        this.number = number;
        this.faculty = faculty;
        studentList = new ArrayList<>();
    }

    public void addStudent(Practice5.Student student) {
        studentList.add(student);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    @Override
    public String toString() {
        return "StudentGroup{" +
                "faculty='" + faculty + '\'' +
                ", number=" + number +
                ", studentList=" + studentList +
                '}';
    }
}
