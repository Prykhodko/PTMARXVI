package HW2;

import junit.framework.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by TROUBLE on 26.04.16.
 */
public class StringUtilsTest {

    @Test
    public void testReverse() throws Exception {
        String text1 = "simple text";
        String text2 = "Hello world!";
        String text3 = "кириллический текст";
        org.junit.Assert.assertEquals("Проверка теста Reverse: " + text1, "txet elpmis", StringUtils.reverse(text1));
        org.junit.Assert.assertEquals("Проверка теста Reverse: " + text2, "!dlrow olleH", StringUtils.reverse(text2));
        org.junit.Assert.assertEquals("Проверка теста Reverse: " + text3, "тскет йиксечиллирик", StringUtils.reverse(text3));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReverseExc() throws Exception {
        String text0 = "";
        org.junit.Assert.assertEquals("Проверка теста Reverse: ", "Проверка теста Reverse: ", StringUtils.reverse(text0));
    }

    @Test
    public void testIsPalindrome() throws Exception {
        String text1 = "А роза упала на лапу Азора";
        String text2 = "Я иду с мечем судия";
        String text3 = "Лидер Венере не вредил";
        String text4 = "Казак";
        String text5 = "Madam, I’m Adam";
        String text6 = "Этот текст не палиндром";
        String text7 = "Слово";
        org.junit.Assert.assertEquals("Проверка теста isPalindrome: " + text1, true, StringUtils.isPalindrome(text1));
        org.junit.Assert.assertEquals("Проверка теста isPalindrome: " + text2, true, StringUtils.isPalindrome(text2));
        org.junit.Assert.assertEquals("Проверка теста isPalindrome: " + text3, true, StringUtils.isPalindrome(text3));
        org.junit.Assert.assertEquals("Проверка теста isPalindrome: " + text4, true, StringUtils.isPalindrome(text4));
        org.junit.Assert.assertEquals("Проверка теста isPalindrome: " + text5, true, StringUtils.isPalindrome(text5));
        org.junit.Assert.assertEquals("Проверка теста isPalindrome: " + text6, false, StringUtils.isPalindrome(text6));
        org.junit.Assert.assertEquals("Проверка теста isPalindrome: " + text7, false, StringUtils.isPalindrome(text7));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsPalindromeExc() throws Exception {
        String text0 = "";
        org.junit.Assert.assertEquals("Проверка теста isPalindrome: ", "Проверка теста isPalindrome: ", StringUtils.isPalindrome(text0));
    }

    @Test
    public void testCheckLength() throws Exception {
        String text0 = "";
        String text1 = "Simple";
        String text2 = "Simple text with more than 10 symbols";
        org.junit.Assert.assertEquals("Проверка теста checkLength: " + text0, "oooooooooooo", StringUtils.checkLength(text0));
        org.junit.Assert.assertEquals("Проверка теста checkLength: " + text1, "Simpleoooooo", StringUtils.checkLength(text1));
        org.junit.Assert.assertEquals("Проверка теста checkLength: " + text2, "Simple", StringUtils.checkLength(text2));
    }

    @Test
    public void testReverseFirstLastWords() throws Exception {
        String text1 = "Simple text";
        String text2 = "Very hard text with difficulties for testing";
        String text3 = "Кириллический текст тупо для прикола";
        org.junit.Assert.assertEquals("Проверка теста reverseFirstLastWords: " + text1, "text Simple", StringUtils.reverseFirstLastWords(text1));
        org.junit.Assert.assertEquals("Проверка теста reverseFirstLastWords: " + text2, "testing hard text with difficulties for Very", StringUtils.reverseFirstLastWords(text2));
        org.junit.Assert.assertEquals("Проверка теста reverseFirstLastWords: " + text3, "прикола текст тупо для Кириллический", StringUtils.reverseFirstLastWords(text3));
    }

    @Test(expected = IllegalArgumentException.class)
    public void reverseFirstLastWordsExc() throws Exception {
        String text0 = "";
        org.junit.Assert.assertEquals("Проверка теста reverseFirstLastWords: ", "Проверка теста reverseFirstLastWords: ", StringUtils.reverseFirstLastWords(text0));
    }

    @Test
    public void testReverseFirstLastWordsSentence() throws Exception {
        String text1 = "Simple text.";
        String text2 = "Very hard text with difficulties for testing. It is a second sentence.";
        String text3 = "Кириллический текст тупо для прикола. Нужно добавить еще предложение для проверки.";
        org.junit.Assert.assertEquals("Проверка теста reverseFirstLastWordsSentence: " + text1, "text Simple.", StringUtils.reverseFirstLastWordsSentence(text1));
        org.junit.Assert.assertEquals("Проверка теста reverseFirstLastWordsSentence: " + text2, "testing hard text with difficulties for Very. sentence is a second It.", StringUtils.reverseFirstLastWordsSentence(text2));
        org.junit.Assert.assertEquals("Проверка теста reverseFirstLastWordsSentence: " + text3, "прикола текст тупо для Кириллический. проверки добавить еще предложение для Нужно.", StringUtils.reverseFirstLastWordsSentence(text3));
    }

    @Test(expected = IllegalArgumentException.class)
    public void reverseFirstLastWordsSentenceExc() throws Exception {
        String text0 = "";
        org.junit.Assert.assertEquals("Проверка теста reverseFirstLastWordsSentence: ", "Проверка теста reverseFirstLastWordsSentence: ", StringUtils.reverseFirstLastWordsSentence(text0));
    }

    @Test
    public void testIsContainabc() throws Exception {
        String text1 = "a";
        String text2 = "b";
        String text3 = "c";
        String text4 = "aaabbbaababccbbabababc";
        String text5 = "f";
        String text6 = "stringwithalofsymbols";
        org.junit.Assert.assertEquals("Проверка теста isContainabc: " + text1, true, StringUtils.isContainabc(text1));
        org.junit.Assert.assertEquals("Проверка теста isContainabc: " + text2, true, StringUtils.isContainabc(text2));
        org.junit.Assert.assertEquals("Проверка теста isContainabc: " + text3, true, StringUtils.isContainabc(text3));
        org.junit.Assert.assertEquals("Проверка теста isContainabc: " + text4, true, StringUtils.isContainabc(text4));
        org.junit.Assert.assertEquals("Проверка теста isContainabc: " + text5, false, StringUtils.isContainabc(text5));
        org.junit.Assert.assertEquals("Проверка теста isContainabc: " + text6, false, StringUtils.isContainabc(text6));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsContainabcExc() throws Exception {
        String text0 = "";
        org.junit.Assert.assertEquals("Проверка теста isContainabc: ", "Проверка теста isContainabc: ", StringUtils.isContainabc(text0));
    }

    @Test
    public void testIsDate() throws Exception {
        String text1 = "04.36.2015";
        String text2 = "04.13.1985";
        String text3 = "01.01.1900";
        String text4 = "02.31.1956";
        String text5 = "4.14.1987";
        String text6 = "13.14.2051";
        String text7 = "11.25.1567";
        String text8 = "10.13.2456";
        String text9 = "10/05/1986";
        org.junit.Assert.assertEquals("Проверка теста isDate: " + text1, false, StringUtils.isDate(text1));
        org.junit.Assert.assertEquals("Проверка теста isDate: " + text2, true, StringUtils.isDate(text2));
        org.junit.Assert.assertEquals("Проверка теста isDate: " + text3, true, StringUtils.isDate(text3));
        org.junit.Assert.assertEquals("Проверка теста isDate: " + text4, true, StringUtils.isDate(text4));
        org.junit.Assert.assertEquals("Проверка теста isDate: " + text5, true, StringUtils.isDate(text5));
        org.junit.Assert.assertEquals("Проверка теста isDate: " + text6, false, StringUtils.isDate(text6));
        org.junit.Assert.assertEquals("Проверка теста isDate: " + text7, false, StringUtils.isDate(text7));
        org.junit.Assert.assertEquals("Проверка теста isDate: " + text8, false, StringUtils.isDate(text8));
        org.junit.Assert.assertEquals("Проверка теста isDate: " + text9, false, StringUtils.isDate(text9));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsDateExc() throws Exception {
        String text0 = "";
        org.junit.Assert.assertEquals("Проверка теста isDate: ", "Проверка теста isDate: ", StringUtils.isDate(text0));
    }

    @Test
    public void testIsEmail() throws Exception {
        String text1 = "trouble-nikita@mail.ru";
        String text2 = "nomail.com";
        String text3 = "prykhodko.Mykyta@gmail.com";
        String text4 = "nomailwithoutdot";
        String text5 = ".wrong_email@email.com";
        String text6 = "wright.email@mail.us";
        org.junit.Assert.assertEquals("Проверка теста isEmail: " + text1, true, StringUtils.isEmail(text1));
        org.junit.Assert.assertEquals("Проверка теста isEmail: " + text2, false, StringUtils.isEmail(text2));
        org.junit.Assert.assertEquals("Проверка теста isEmail: " + text3, true, StringUtils.isEmail(text3));
        org.junit.Assert.assertEquals("Проверка теста isEmail: " + text4, false, StringUtils.isEmail(text4));
        org.junit.Assert.assertEquals("Проверка теста isEmail: " + text5, false, StringUtils.isEmail(text5));
        org.junit.Assert.assertEquals("Проверка теста isEmail: " + text6, true, StringUtils.isEmail(text6));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsEmailExc() throws Exception {
        String text0 = "";
        org.junit.Assert.assertEquals("Проверка теста testIsEmailExc: ", "Проверка теста testIsEmailExc: ", StringUtils.isEmail(text0));
    }

    @Test
    public void testFindPhones() throws Exception {
        String text1 = "В этом тексте можно найти мой номер телефона 80504242772. Текст кириллицей.";
        String text2 = "English text that contains another phone number 80504730967";
        String text3 = "That text has 2 numbers - my(80504242772) and Lizas (80504260235)";
        String text4 = "text withount any phone numbers";
        String[] text1result = {"+8(050)424-27-72"};
        String[] text2result = {"+8(050)473-09-67"};
        String[] text3result = {"+8(050)424-27-72", "+8(050)426-02-35"};
        String[] text4result = new String[0];
        org.junit.Assert.assertEquals("Проверка теста FindPhones: " + text1, text1result, StringUtils.findPhones(text1));
        org.junit.Assert.assertEquals("Проверка теста FindPhones: " + text2, text2result, StringUtils.findPhones(text2));
        org.junit.Assert.assertEquals("Проверка теста FindPhones: " + text3, text3result, StringUtils.findPhones(text3));
        org.junit.Assert.assertEquals("Проверка теста FindPhones: " + text4, text4result, StringUtils.findPhones(text4));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindPhonesExc() throws Exception {
        String text0 = "";
        org.junit.Assert.assertEquals("Проверка теста testFindPhonesExc: ", "Проверка теста testFindPhonesExc: ", StringUtils.findPhones(text0));
    }
}