package Practice1;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by TROUBLE on 14.05.16.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(value = {ArraySumTest.class, ArrayProdTest.class})
public class AllTests {
}
