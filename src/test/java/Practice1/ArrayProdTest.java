package Practice1;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by TROUBLE on 14.05.16.
 */
public class ArrayProdTest {

    @Test
    public void testProd() throws Exception {
        int actual = ArrayProd.prod(new int[] {1,2,3,3,3});
        int expected = 54;
        Assert.assertEquals("Wrong prod", expected, actual, 0.001);
    }
}