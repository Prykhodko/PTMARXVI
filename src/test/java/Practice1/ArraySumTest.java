package Practice1;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by TROUBLE on 14.05.16.
 */
public class ArraySumTest {

    @Test
    public void testSum() throws Exception {
        int actual = ArraySum.sum(new int[] {2,3,4,5});
        int expected = 14;
        Assert.assertEquals("Wrong summurizing", expected, actual, 0.001);
    }
}