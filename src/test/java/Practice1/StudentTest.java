package Practice1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by TROUBLE on 16.05.16.

 Разработка, основанная на тестах
 Разработайте на основе тестов один из следующих классов.
 Разработать класс, представляющий студента. Студент характеризуется
 именем, фамилией, группой и набором экзаменов, которые он сдавал.
 Экзамен характеризуется названием предмета, оценкой студента по нему и
 датой сдачи (год, семестр). Группа характеризуется курсом и факультетом.
 Необходимые операции таковы:
 - узнать наивысшую оценку среди всех экзаменов по данному предмету
 - добавить ему оценку по экзамену
 - удалить для него оценку по экзамену;
 - если он такой экзамен не сдавал - сгенерировать исключение
 - узнать число экзаменов, которые он сдал с указанной оценкой
 - узнать его средний балл за указанный семестр;
 */
public class StudentTest {

    //N.B. Очень важно! Перед тестированием надо установить модификатор доступа studentList как public!

    Group groupMEO3 = new Group(3, Faculty.MEO);
    Group groupMEO4 = new Group(4, Faculty.MEO);

    Exam reExamPrykhodko = new Exam(Lecture.LOGIC, 2014, 8, Semester.FIRST);
    Exam invalidExam = new Exam(Lecture.GEOMETRY, 2014, 9, Semester.FIRST);

    Exam forAverageMark1 = new Exam(Lecture.ASTRONOMY, 2014, 8, Semester.FIRST);
    Exam forAverageMark2 = new Exam(Lecture.BIOLOGY, 2014, 8, Semester.FIRST);

    Student student1 = new Student("Mykyta", "Prykhodko", groupMEO3, new ArrayList<Exam>(Arrays.asList(
            new Exam(Lecture.DRAWING, 2014, 7, Semester.FIRST),
            new Exam(Lecture.ECONOMY, 2014, 11, Semester.SECOND),
            new Exam(Lecture.LITERATURE, 2015, 9, Semester.FIRST),
            new Exam(Lecture.MATH, 2015, 10, Semester.SECOND),
            new Exam(Lecture.PHYSICS, 2016, 11, Semester.FIRST)
    )));

    Student student2 = new Student("Illy", "Serikov", groupMEO3, new ArrayList<Exam>(Arrays.asList(
            new Exam(Lecture.DRAWING, 2014, 5, Semester.FIRST),
            new Exam(Lecture.ECONOMY, 2014, 4, Semester.SECOND),
            new Exam(Lecture.LITERATURE, 2015, 7, Semester.FIRST),
            new Exam(Lecture.MATH, 2015, 4, Semester.SECOND),
            new Exam(Lecture.PHYSICS, 2016, 6, Semester.FIRST)
    )));

    Student student3 = new Student("Andrey", "Serdykov", groupMEO4, new ArrayList<Exam>(Arrays.asList(
            new Exam(Lecture.DRAWING, 2014, 8, Semester.FIRST),
            new Exam(Lecture.ECONOMY, 2014, 10, Semester.SECOND),
            new Exam(Lecture.LITERATURE, 2015, 5, Semester.FIRST),
            new Exam(Lecture.MATH, 2015, 11, Semester.SECOND),
            new Exam(Lecture.PHYSICS, 2016, 9, Semester.FIRST)
    )));

    Student student4 = new Student("Dima", "Dmitriev", groupMEO4, new ArrayList<Exam>(Arrays.asList(
            new Exam(Lecture.DRAWING, 2014, 10, Semester.FIRST),
            new Exam(Lecture.ECONOMY, 2014, 11, Semester.SECOND),
            new Exam(Lecture.LITERATURE, 2015, 6, Semester.FIRST),
            new Exam(Lecture.MATH, 2015, 5, Semester.SECOND),
            new Exam(Lecture.PHYSICS, 2016, 9, Semester.FIRST)
    )));

    Student student5 = new Student("Larisa", "Dolina", groupMEO3, new ArrayList<Exam>(Arrays.asList(
            new Exam(Lecture.DRAWING, 2014, 11, Semester.FIRST),
            new Exam(Lecture.ECONOMY, 2014, 11, Semester.SECOND),
            new Exam(Lecture.LITERATURE, 2015, 5, Semester.FIRST),
            new Exam(Lecture.MATH, 2015, 10, Semester.SECOND),
            new Exam(Lecture.PHYSICS, 2016, 9, Semester.FIRST)
    )));

    Student student6 = new Student("Nikolay", "Agutin", groupMEO4, new ArrayList<Exam>(Arrays.asList(
            new Exam(Lecture.DRAWING, 2014, 6, Semester.FIRST),
            new Exam(Lecture.ECONOMY, 2014, 8, Semester.SECOND),
            new Exam(Lecture.LITERATURE, 2015, 7, Semester.FIRST),
            new Exam(Lecture.MATH, 2015, 5, Semester.SECOND),
            new Exam(Lecture.PHYSICS, 2016, 4, Semester.FIRST)
    )));

    Student student7 = new Student("Alla", "Pugachova", groupMEO3, new ArrayList<Exam>(Arrays.asList(
            new Exam(Lecture.DRAWING, 2014, 8, Semester.FIRST),
            new Exam(Lecture.ECONOMY, 2014, 9, Semester.SECOND),
            new Exam(Lecture.LITERATURE, 2015, 10, Semester.FIRST),
            new Exam(Lecture.MATH, 2015, 11, Semester.SECOND),
            new Exam(Lecture.PHYSICS, 2016, 5, Semester.FIRST)
    )));

    Student student8 = new Student("Inna", "Varum", groupMEO4, new ArrayList<Exam>(Arrays.asList(
            new Exam(Lecture.DRAWING, 2014, 8, Semester.FIRST),
            new Exam(Lecture.ECONOMY, 2014, 9, Semester.SECOND),
            new Exam(Lecture.LITERATURE, 2015, 10, Semester.FIRST),
            new Exam(Lecture.MATH, 2015, 11, Semester.SECOND),
            new Exam(Lecture.PHYSICS, 2016, 6, Semester.FIRST)
    )));

    Student student9 = new Student("Andrey", "Petrenko", groupMEO3, new ArrayList<Exam>(Arrays.asList(
            new Exam(Lecture.DRAWING, 2014, 6, Semester.FIRST),
            new Exam(Lecture.ECONOMY, 2014, 8, Semester.SECOND),
            new Exam(Lecture.LITERATURE, 2015, 9, Semester.FIRST),
            new Exam(Lecture.MATH, 2015, 5, Semester.SECOND),
            new Exam(Lecture.PHYSICS, 2016, 10, Semester.FIRST)
    )));

    Student student10 = new Student("Vlad", "Povh", groupMEO4, new ArrayList<Exam>(Arrays.asList(
            new Exam(Lecture.DRAWING, 2014, 8, Semester.FIRST),
            new Exam(Lecture.ECONOMY, 2014, 9, Semester.SECOND),
            new Exam(Lecture.LITERATURE, 2015, 10, Semester.FIRST),
            new Exam(Lecture.MATH, 2015, 11, Semester.SECOND),
            new Exam(Lecture.PHYSICS, 2016, 7, Semester.FIRST)
    )));

    List<Student> studentList = new ArrayList<>(Arrays.asList(
           student1, student2, student3, student4, student5, student6, student7, student8, student9, student10
    ));

    //узнать наивысшую оценку среди всех экзаменов по данному предмету
    @Test
    public void testMaxMark() throws Exception {
        int actual = Student.maxMark(Lecture.PHYSICS, studentList);
        int expected = 11;
        Assert.assertEquals("return wrong max mark from students lists", expected, actual);
    }

    @Test
    public void testAddExamMark() throws Exception {
        student1.addExamMark(reExamPrykhodko);
        Assert.assertTrue(student1.examList.contains(reExamPrykhodko));
    }

    @Test
    public void testDeleteExamMark() throws Exception {
        student1.addExamMark(reExamPrykhodko);
        Assert.assertTrue(student1.examList.contains(reExamPrykhodko));
        student1.deleteExamMark(reExamPrykhodko);
        Assert.assertFalse(student1.examList.contains(reExamPrykhodko));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testDeleteExamMarkException() throws Exception {
        student1.deleteExamMark(invalidExam);
        Assert.assertFalse(student1.examList.contains(reExamPrykhodko));
    }

    @Test
    public void testExamQuantity() throws Exception {
        int actual1 = student1.examQuantity(11);
        int actual2 = student1.examQuantity(4);
        int expected1 = 2;
        int expected2 = 0;
        Assert.assertEquals("Wrong quantitiy of exam with some mark", expected1, actual1);
        Assert.assertEquals("Wrong quantitiy of exam with some mark", expected2, actual2);
    }

    @Test
    public void testAverageMarkForSemester() throws Exception {
        student1.addExamMark(forAverageMark1);
        student1.addExamMark(forAverageMark2);
        double actual = student1.averageMarkForSemester(2014, Semester.FIRST);
        double expected = 7.67;
        Assert.assertEquals("Wrong average mark", expected, actual, 0.001);
    }
}